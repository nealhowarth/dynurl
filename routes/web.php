<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');

Route::get('/campaigns', 'CampaignsController@home');
Route::get('/campaigns/create/{uuid}', 'CampaignsController@createDynamics');
Route::get('/campaign/create', 'CampaignsController@create');
Route::get('/campaign/edit/{uuid}', 'CampaignsController@edit');
Route::get('/campaign/edit/{uuid}/dynamics', 'CampaignsController@editDynamics');

Route::get('/campaign/{uuid}/stats', 'CampaignStatsController@home');
Route::get('/campaigns/stats/test', 'CampaignStatsController@test');


// catch-all route for tiny urls
Route::get('/{url}', 'DecisionController@index');
