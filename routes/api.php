<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register the API routes for your application as
| the routes are automatically authenticated using the API guard and
| loaded automatically by this application's RouteServiceProvider.
|
*/

Route::group([
    'middleware' => 'auth:api'
], function () {
    
    // Campaigns
    Route::get('/campaign/{uuid}', 'CampaignsController@getCampaign');
    Route::put('/campaign/{uuid}/stop', 'CampaignsController@stopCampaign');
    Route::put('/campaign/{uuid}/start', 'CampaignsController@startCampaign');
    Route::put('/campaign/{uuid}/delete', 'CampaignsController@deleteCampaign');
    Route::post('/campaign/create', 'CampaignsController@store');
    Route::post('/campaign/create/{uuid}', 'CampaignsController@storeDynamics');
    Route::get('/campaigns/all', 'CampaignsController@all');
    Route::post('/campaign/edit/{uuid}', 'CampaignsController@updateCampaign');
    
    Route::get('/campaign/{uuid}/stats/week', 'CampaignStatsController@getWeeklyClickStats');
    Route::post('/campaign/{uuid}/stats/all', 'CampaignStatsController@getAllClickStats');
    Route::get('/campaign/{uuid}/stats', 'CampaignStatsController@getCampaignStats');
    
	Route::put('/notifications/{id}/delete', 'HomeController@deleteNotification');

    Route::get('/settings/pixels', 'Settings\PixelsController@getPixels');
    Route::post('/settings/pixels', 'Settings\PixelsController@storePixels');

});
