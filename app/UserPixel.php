<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPixel extends Model
{
    

    protected $fillable = [
        'user_id', 'type', 'pixel'
    ];



    public function user()
    {
        return $this->belongsTo('App\User');
    }


}
