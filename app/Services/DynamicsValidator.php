<?php

namespace App\Services;


class DynamicsValidator
{
    

	private $messages;


	public function __construct()
	{
		$this->messages = [
			'apercent.required' => 'The A percent field is required',
			'apercent.numeric' => 'The A percent field must be a number',
			'aurl.required' => 'The A URL field is required',
			'aurl.url' => 'The A URL field must contain a valid URL',
			'bpercent.required' => 'The B percent field is required',
			'bpercent.numeric' => 'The B percent field must be a number',
			'burl.required' => 'The B URL field is required',
			'burl.url' => 'The B URL field must contain a valid URL',
			'defaultUrl.required' => 'The default URL is required',
			'defaultUrl.url' => 'The default URL must be a valid URL',
			'devices.required' => 'At least 1 device type is required',
			'devices.min' => 'At least 1 device type is required',
			'devices.*.type.required' => 'Device type is required',
			'devices.*.url.required' => 'URL is required',
			'devices.*.url.url' => 'A valid URL is required',
		];
	}
	
	
	public function validateAbTest($data)
	{

		return \Validator::make($data, [
			'apercent' => 'required|numeric',
			'aurl' => 'required|url',
			'bpercent' => 'required|numeric',
			'burl' => 'required|url'
		], $this->messages);

	}


	public function validateDeviceType($data)
	{

		return \Validator::make($data, [
			'defaultUrl' => 'required|url',
			'devices' => 'required|array|min:1',
			'devices.*.type' => 'required',
			'devices.*.url' => 'required|url'
		], $this->messages);

	}


	public function validateBrowserLanguage($data)
	{

		return \Validator::make($data, [
			'defaultUrl' => 'required|url',
			'languages' => 'required|array|min:1',
			'languages.*.type' => 'required',
			'languages.*.url' => 'required|url'
		], $this->messages);

	}


	public function validateLocation($data)
	{

		return \Validator::make($data, [
			'defaultUrl' => 'required|url',
			'locations' => 'required|array|min:1',
			'locations.*.type' => 'required',
			'locations.*.url' => 'required|url'
		], $this->messages);

	}


	public function validateVisitorLimit($data)
	{

		return \Validator::make($data, [
			'defaultUrl' => 'required|url',
			'limit' => 'required|numeric',
			'limiturl' => 'required|url'
		], $this->messages);

	}


	public function validateDateLimit($data)
	{

		return \Validator::make($data, [
			'defaultUrl' => 'required|url',
			'startDate' => 'required',
			'endDate' => 'required',
			'visitUrl' => 'required|url'
		], $this->messages);

	}



}