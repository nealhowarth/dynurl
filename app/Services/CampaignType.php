<?php

namespace App\Services;

use App\Services\Dynamics\Abtest;
use App\Services\Dynamics\Location;
use App\Services\Dynamics\DateLimit;
use App\Services\Dynamics\DeviceType;
use App\Services\Dynamics\VisitorLimit;
use App\Services\Dynamics\BrowserLanguage;

class CampaignType
{
	
	private $type;

	private $service;


	public function __construct($campaignType)
	{
		$this->type = $campaignType;

		$this->service = $this->loadService($campaignType);
	}


	/**
	 * Load the service class
	 *
	 * @param string $campaignType
	 */
	public function loadService($campaignType)
	{
		
		switch ($campaignType) {
			case 'abtest':
				return new Abtest();
				break;

			case 'devicetype':
				return new DeviceType();
				break;

			case 'browserlanguage':
				return new BrowserLanguage();
				break;

			case 'location':
				return new Location();
				break;

			case 'visitorlimit':
				return new VisitorLimit();
				break;

			case 'datelimit':
				return new DateLimit();
				break;
			
		}

	}


	/**
	 * Call the service to make the decision
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
		return $this->service->makeDecision($campaign, $visitorData);
	}


}
