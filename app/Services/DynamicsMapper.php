<?php

namespace App\Services;

class DynamicsMapper
{
    

    public function mapAbTest($data)
    {
        
        // validate
        if( !isset($data['apercent']) ||
            !isset($data['aurl']) ||
            !isset($data['bpercent']) ||
            !isset($data['burl'])
        ) {
            return false;
        }

        $returnData = [
            'type' => 'abtest',
            'apercent' => $data['apercent'],
            'aurl' => $data['aurl'],
            'bpercent' => $data['bpercent'],
            'burl' => $data['burl']
        ];

        return json_encode($returnData, TRUE);

	}
	

	public function mapDeviceType($data)
    {
        
        // validate
		if( !isset($data['defaultUrl']) ) return false;
		
		foreach($data['devices'] as $device) {

			if( !isset($device['type']) || $device['type'] == '' ) return false;
			if( !isset($device['url']) || $device['url'] == '' ) return false;

		}
            

        $returnData = [
			'type' => 'devicetype',
			'defaultUrl' => $data['defaultUrl'],
            'devices' => $data['devices']
        ];

        return json_encode($returnData, TRUE);

    }


    public function mapBrowserLanguage($data)
    {
        if( !isset($data['defaultUrl']) ) return false;

        foreach($data['languages'] as $language) {

			if( !isset($language['type']) || $language['type'] == '' ) return false;
			if( !isset($language['url']) || $language['url'] == '' ) return false;

		}
            

        $returnData = [
			'type' => 'browserlanguage',
			'defaultUrl' => $data['defaultUrl'],
            'languages' => $data['languages']
        ];

        return json_encode($returnData, TRUE);

    }


    public function mapLocation($data)
    {
        if( !isset($data['defaultUrl']) ) return false;

        foreach($data['locations'] as $location) {

			if( !isset($location['type']) || $location['type'] == '' ) return false;
			if( !isset($location['url']) || $location['url'] == '' ) return false;

		}
            

        $returnData = [
			'type' => 'location',
			'defaultUrl' => $data['defaultUrl'],
            'locations' => $data['locations']
        ];

        return json_encode($returnData, TRUE);

    }


    public function mapVisitorLimit($data)
    {
        
        // validate
        if( !isset($data['defaultUrl']) ) return false;
        
        if( !isset($data['limit']) ||
            !isset($data['url'])
        ) {
            return false;
        }

        $returnData = [
            'type' => 'visitorlimit',
            'defaultUrl' => $data['defaultUrl'],
            'limit' => $data['limit'],
            'limiturl' => $data['limiturl']
        ];

        return json_encode($returnData, TRUE);

	}


	public function mapDateLimit($data)
    {
        
        // validate
        if( !isset($data['defaultUrl']) ) return false;
        
        if( !isset($data['startDate']) ||
			!isset($data['endDate']) || 
			!isset($data['visitUrl'])
        ) {
            return false;
        }

        $returnData = [
            'type' => 'datelimit',
            'defaultUrl' => $data['defaultUrl'],
            'startDate' => $data['startDate'],
			'endDate' => $data['endDate'],
			'visitUrl' => $data['visitUrl']
        ];

        return json_encode($returnData, TRUE);

	}


}
