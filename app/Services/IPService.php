<?php

namespace App\Services;

/**
 * Example response
 * {
 * "ip":8.8.8.8
 * "continent_name":North America
 * "country_name":United States 
 * "country_capital":Washington 
 * "state_prov":California 
 * "district":Santa Clara County 
 * "city":Mountain View 
 * "zipcode":94043 
 * "latitude":37.4229 
 * "longitude":-122.085 
 * "is_eu":false 
 * "calling_code":+1 
 * "country_tld":.us 
 * "languages":en-US,es-US,haw,fr 
 * "country_flag":https://ipgeolocation.io/static/flags/us_64.png
 * }
 */
class IPService
{

    private $apiKey;
    
    /**
     * @var string
     */
    private $apiUrl = "https://api.ipgeolocation.io/ipgeo?apiKey=%s&ip=%s";


    public function __construct()
    {
        $this->apiKey = env('IP_LOCATION_API_KEY');
    }


    public function getLatLonByIp($ip)
    {
        $response = $this->fetch($ip);

        $data = json_decode($response, true);

        if(!isset($data['latitude'])) {
            return false;//throw new \Exception($data['message']);
        }

        return [
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'country' => $data['country_name'],
            'city' => $data['city'],
            'zipcode' => $data['zipcode'],
            'country_code2' => $data['country_code2']
        ];
    }


    /** 
     * Converts an IP address into location data
     * 
     * @param string $ip
     * @return string
     */
    private function fetch($ip)
    {
        $url = sprintf($this->apiUrl, $this->apiKey, $ip);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}
