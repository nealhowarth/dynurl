<?php

namespace App\Services\Dynamics;


class BrowserLanguage
{
	
	/**
	 * Match the browser language to the URL
	 *
	 * @param campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
		$data = json_decode($campaign->data);
		
		// check for a device type match
		foreach($data->languages as $language) {

			if($language->type == $visitorData['language_code']) {
				return $language->url;
			}

		}

		// otherwise return the default URL if no matches
		return $data->defaultUrl;

	}


}
