<?php

namespace App\Services\Dynamics;


class DateLimit
{
	
	/**
	 * Checks the date range of the campaign for whether today's date is included
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
        
        $data = json_decode($campaign->data);
        
		$startDate = \DateTime::createFromFormat('Y-m-d', substr($data->startDate, 0, -14));
		$endDate = \DateTime::createFromFormat('Y-m-d', substr($data->endDate, 0, -14));
		$url = $data->visitUrl;

		$today = new \DateTime();

		// is today in the date range
		if($today >= $startDate && $today <= $endDate) {
			return $url;
		}

		// otherwise return the default URL if no matches
		return $data->defaultUrl;

	}


}
