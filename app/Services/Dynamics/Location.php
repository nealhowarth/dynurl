<?php

namespace App\Services\Dynamics;

use App\Services\Cache\IPCacheService;
use App\Services\IPService;


class Location
{
	
	/**
	 * Match the location to the URL
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
		
		$data = json_decode($campaign->data);
		
		$ip = $visitorData['ip_address'];
		
		// check the IP in the cache
		$cacheService = new IPCacheService();
		$ipData = $cacheService->exists($ip);
		
		if( $ipData ) {

			$userData = json_decode($ipData->data);

			// check for a device type match
			foreach($data->locations as $location) {
				
				if( strtoupper($location->type) == strtoupper($userData->country_code2) ) {
					return $location->url;
				}

			}
		
		}

		// otherwise return the default URL if no matches
		return $data->defaultUrl;

	}


}
