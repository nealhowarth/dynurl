<?php

namespace App\Services\Dynamics;

use Namshi\AB\Test;

class Abtest
{
	
	/**
	 * Currently supports A & B test
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
		$data = json_decode($campaign->data);

		$result = new Test('ab_test', array(
			$data->aurl => $data->apercent,
			$data->burl => $data->bpercent,
		));

		return $result->getVariation();

	}


}
