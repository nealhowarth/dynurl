<?php

namespace App\Services\Dynamics;


class DeviceType
{
	
	/**
	 * Match the device type to the URL
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
		$data = json_decode($campaign->data);
		
		// check for a device type match
		foreach($data->devices as $device) {

			// group mobile and phone as one option
			if($visitorData['device_type'] == 'mobile' || $visitorData['device_type'] == 'phone') {
				$visitorData['device_type'] = 'phone';
			}

			if($device->type == $visitorData['device_type']) {
				return $device->url;
			}

		}

		// otherwise return the default URL if no matches
		return $data->defaultUrl;

	}


}
