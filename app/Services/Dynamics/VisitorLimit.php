<?php

namespace App\Services\Dynamics;

use App\CampaignVisit;

class VisitorLimit
{
	
	/**
	 * Checks the user data to see if they have clicked the campaign link before
     * The check is based on a user id being stored in a cookie, which is not exactly foolproof
	 *
	 * @param Campaign $campaign
	 * @param array $visitorData
	 * @return string
	 */
	public function makeDecision($campaign, $visitorData)
	{
        
        $data = json_decode($campaign->data);
        
        $visitCount = CampaignVisit::where('campaign_id', $campaign->id)->where('visitor_id', $visitorData['visitorId'])->count();
        
        if(!$visitCount) {
            $visitCount = 0;
        }
        
        
        // have we reached the visit limit for this user
        if($visitCount <= $data->limit) {
            return $data->limiturl;
        }
        
        // otherwise return the default URL if no matches
		return $data->defaultUrl;

	}


}
