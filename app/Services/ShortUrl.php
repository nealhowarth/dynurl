<?php

namespace App\Services;


class ShortUrl
{
    
    public function generate(int $length = 6)
    {
        $characters = str_repeat('abcdefghijklmnopqrstuvwxyz0123456789', $length);

        return substr(str_shuffle($characters), 0, $length);
    }


}
