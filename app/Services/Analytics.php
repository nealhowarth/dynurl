<?php

namespace App\Services;

use Auth;
use Illuminate\Http\Request;

use App\Campaign;
use App\CampaignVisit;
use App\CampaignChange;


class Analytics
{
    
    public function getClicksByDateRange($campaign, $startDate, $endDate)
    {
        $clicksRows = \DB::select(\DB::raw('
			SELECT count(id) as clicks, DATE(created_at) as name
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) BETWEEN :startDate AND :endDate 
			GROUP BY DATE(created_at)
			ORDER BY DATE(created_at) ASC
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        $clicks = $this->formatValues($clicksRows, $startDate, $endDate);
        
        return $clicks;
    }


    public function getUniqueVisitorsByDateRange($campaign, $startDate, $endDate)
    {
        $clicksRows = \DB::select(\DB::raw('
			SELECT count(distinct(ip_address)) as clicks
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) >= :startDate 
            AND DATE(created_at) <= :endDate 
			GROUP BY ip_address
			ORDER BY DATE(created_at) ASC
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        $clicks = $this->formatValues($clicksRows, $startDate, $endDate);

        return $clicks;
    }


    public function getBrowsersByDateRange($campaign, $startDate, $endDate)
    {
        $clicks = \DB::select(\DB::raw('
			SELECT count(id) as clicks, browser_type
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) >= :startDate 
            AND DATE(created_at) <= :endDate 
			GROUP BY browser_type
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        return $clicks;
    }


    public function getOSByDateRange($campaign, $startDate, $endDate)
    {
        $clicks = \DB::select(\DB::raw('
			SELECT count(id) as clicks, os
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) >= :startDate 
            AND DATE(created_at) <= :endDate 
			GROUP BY os
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        return $clicks;
    }


    public function getDevicesByDateRange($campaign, $startDate, $endDate)
    {
        $clicks = \DB::select(\DB::raw('
			SELECT count(id) as clicks, device_type
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) >= :startDate 
            AND DATE(created_at) <= :endDate 
			GROUP BY device_type
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        return $clicks;
    }


    public function getBrowserLanguageByDateRange($campaign, $startDate, $endDate)
    {
        $clicks = \DB::select(\DB::raw('
			SELECT count(id) as clicks, language_description
            FROM campaign_visits 
            WHERE campaign_id = :id 
            AND DATE(created_at) >= :startDate 
            AND DATE(created_at) <= :endDate 
			GROUP BY language_description
            '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

        return $clicks;
    }


    public function getReferersByDateRange($campaign, $startDate, $endDate)
    {
        # code...
    }


    public function getCountriesByDateRange($campaign, $startDate, $endDate)
    {
        # code...
    }


    private function formatValues($data, $startDate, $endDate)
    {
        $start = new \Carbon\Carbon($startDate);
        $end = new \Carbon\Carbon($endDate);

        $clicks = array();

        while($start->lte($end)) {
            
            $itemKey = null;

            foreach($data as $key => $value) {
                if($value->name == $start->format('Y-m-d')) {
                    $itemKey = $key;
                }
            }
            

            if($itemKey !== NULL) {
                $clicks[] = [
                    'name' => $start->format('Y-m-d'),
                    'clicks' => $data[$itemKey]->clicks
                ];
            } else {
                $clicks[] = [
                    'name' => $start->format('Y-m-d'),
                    'clicks' => 0
                ];
            }
            
            $start->addDay();

        }

        return $clicks;

    }

}
