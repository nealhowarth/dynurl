<?php

namespace App\Services\Cache;

use App\IPCache;
use App\Services\IPService;

class IPCacheService
{

    /**
     * Cache refresh limit in seconds
     * @var int
     */
    private $cacheLimit;

    /**
     * The IP service
     */
    private $ipService;


    public function __construct()
    {
        
        $this->cacheLimit = env('IP_CACHE_LIMIT', 259200);

        $this->ipService = new IPService();

    }


    /**
     * Check if the IP has been recorded and the created_at date
     * is less than the cache limit
     */
    public function exists($ip)
    {
        
        $timelimit = new \DateTime();
        $timelimit->add(new \DateInterval('PT'.$this->cacheLimit.'S'));

        // if we are testing locally, 127.0.0.1 will not work, lets use a real on from the env file
        if( env('APP_ENV') == 'local' ) {
            $ip = env('IP_LOCAL_TEST_ADDRESS');
        }

        $record = IPCache::where('ip_address', $ip)->where('created_at', '<=', $timelimit)->first();

        if($record) {
            // return the existing data
            $record->fromCache = true;
            return $record;

        } else {
            // create a new record
            $record = new IPCache;
            $record->ip_address = $ip;

            $ipData = $this->ipService->getLatLonByIp($ip);

            if(!$ipData) {
                return false;
            }

            $record->latitude = $ipData['latitude'];
            $record->longitude = $ipData['longitude'];
            $record->data = json_encode($ipData);

            $record->save();

            $record->fromCache = false;

            return $record;
        }

    }


}
