<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\VisitLimitReached;
use App\Notifications\VisitLimit80Pc;

use App\User;
use App\Team;
use App\Campaign;
use App\CampaignVisit;
use App\Services\CampaignType;
use App\Services\Visitor\VisitorService;
use App\UserPixel;

class DecisionController extends Controller
{
    
    private $visitorService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->visitorService = new VisitorService();
    }


    /**
     * Processes campaign clicks and records visits
     */
    public function index(Request $request, $url)
    {
        if( isset($_COOKIE['dynurl']) ) {
            // we have seen these before
            $visitorId = $_COOKIE['dynurl'];
        } else {
            $visitorId = uniqid('dynurl', true);
        }
        
        if(!$url) {
            return redirect('/home');
        }

        // get the campaign
        $campaign = Campaign::where('url', $url)->first();

        if($campaign == null || $campaign->data == null) {
            return redirect('/home');
        }

        // is the campaign active
        if($campaign->active == 0) {
            return redirect('/home');
        }

        // get the plan details for throttling
        $team = Team::where('id', $campaign->team_id)->first();
        $plan = \Spark::teamPlans()->where('id', $team->current_billing_plan)->first();

        if($team && !$plan) {
            // free plan
            $clickLimit = env('FREE_PLAN_CLICK_LIMIT', 5000);
        } else {
            $clickLimit = $plan->attributes['campaign_clicks'];
        }

        $clickCount = CampaignVisit::where('campaign_id', $campaign->id)->count();
        
        $user = User::where('id', $campaign->team->owner_id)->first();

        // have we reached the click limit
        if( ($clickCount + 1) > $clickLimit) {
			// TODO: send an email to user to say limit reached on campaign
			
			if($user) {
				$user->notify(new VisitLimitReached($campaign));
			}
			
            return redirect( env('DEFAULT_REDIRECT_URL') );
        }

        // if we are at 80% click limit, send a email / message
        if( ($clickCount + 1) == ( (int)$clickLimit * 0.8) ) {

            if($user) {
				$user->notify(new VisitLimit80Pc($campaign, $user));
			}

        }

        // get the visitors details
        $visitorData = $this->visitorService->gatherVisitorData($request);
        $visitorData['visitorId'] = $visitorId;
        
        // perform campaign logic to get URL
		$campaignService = new CampaignType($campaign->type);
		$chosenUrl = $campaignService->makeDecision( $campaign, $visitorData );

        // record click details
        $campaignVisit = new CampaignVisit($visitorData);
        $campaignVisit->campaign_id = $campaign->id;
        $campaignVisit->chosen_url = $chosenUrl;
        $campaignVisit->visitor_id = $visitorId;
        $campaignVisit->save();

        // are there any pixels
        $userPixels = UserPixel::where('user_id', $user->id)->get();
        $pixels = [];

        if($userPixels) {
            
            foreach($userPixels as $px) {

                $pixels[$px->type] = $px->pixel;

            }
        }

        return view('decision', compact('visitorId', 'chosenUrl', 'pixels'));

    }


}