<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Campaign;
use App\CampaignVisit;
use App\CampaignChange;
use App\Services\ShortUrl;
use App\Services\DynamicsMapper;
use App\Services\DynamicsValidator;

class CampaignsController extends Controller
{
    
    private $shortUrlService;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->shortUrlService = new ShortUrl;
		$this->dynamicsMapper = new DynamicsMapper;
		$this->dynamicsValidator = new DynamicsValidator;

        //$this->middleware('subscribed');
    }


    /** 
     * VIEWS
     */

    public function home()
    {
        return view('campaigns.home');
    }

    public function create()
    {
        // check user can create more campaigns
        $plan = \Spark::teamPlans()->where('id', Auth::user()->currentTeam()->current_billing_plan)->first();
        
        if(!$plan) {
            $campaignLimit = 1; // on the free plan
        } else {
            $campaignLimit = $plan->attributes['campaigns'];
        }

        $campaignCount = Campaign::where('team_id', Auth::user()->currentTeam()->id)->count();

        if($campaignCount >= $campaignLimit) {
            \Session::flash('error_message', 'Please <a href="/settings/teams/'.Auth::user()->currentTeam()->id.'#/subscription">upgrade your account</a> to create more campaigns.');
            return redirect('campaigns');
        }
        
        return view('campaigns.create');
    }


    public function createDynamics($uuid)
    {
        if(!$uuid) {
			\Session::flash('error_message', 'Campaign not found');
            return redirect('campaigns');
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();
        
        return view('campaigns.createDynamics');
    }


    public function edit($uuid)
    {
        if(!$uuid) {
			\Session::flash('error_message', 'Campaign not found');
            return redirect('campaigns');
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();
        
        return view('campaigns.edit', compact($campaign));
    }


    public function editDynamics($uuid)
    {
        if(!$uuid) {
			\Session::flash('error_message', 'Campaign not found');
            return redirect('campaigns');
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();
        
        return view('campaigns.editDynamics', compact($campaign));
    }


    


    



    /**
     * API CALLS
     */


    /**
     * Gets a list of all the campaigns for a team
     */
    public function all(Request $request)
    {
        $team = Auth::user()->currentTeam();
        
        $campaigns = Campaign::where('team_id', $team->id)->get();

        // get the last 7 days clicks
        foreach($campaigns as $campaign) {
            // TODO: Move the below into a reporting service
            if($campaign->type == 'abtest') {
                $campaignVisits = $campaignVisits = \DB::select(\DB::raw('
                    SELECT count(id) as total
                    FROM campaign_visits 
                    WHERE campaign_id = :id 
                    GROUP BY chosen_url
                    '), array('id' => $campaign->id));

                foreach($campaignVisits as $visit) {
                    $visits[]['value'] = $visit->total;
                }

                if(empty($visits)) {
                    $visits = 0;
                }

                $campaign->visits = $visits;
                $visits = [];

            } else {

                $campaignVisits = $campaignVisits = \DB::select(\DB::raw('
                    SELECT DATE(created_at), IF(count(id) = 0, 0, count(id)) as total
                    FROM campaign_visits 
                    WHERE campaign_id = :id
                    AND DATE(created_at) >= DATE_SUB(NOW(), INTERVAL 7 day) AND DATE(created_at) <= NOW() 
                    GROUP BY DATE(created_at)
                    '), array('id' => $campaign->id));
                
                $visits[] = 0; // have to pad with a 0 or the graph does not display properly
                foreach($campaignVisits as $visit) {
                    $visits[]['value'] = $visit->total;
                }

                $campaign->visits = $visits;
                $visits = [];
                
            }
            
        }
        
        return $campaigns;
    }


    public function getCampaign($uuid)
    {
        if(!$uuid) {
            return [
                'error' => true,
                'message' => 'Campaign not found'
            ];
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        return [
            'error' => false,
            'message' => null,
            'campaign' => $campaign
        ];

    }


    /**
     * Save a new campaign
     */
    public function store(Request $request)
    {
        return $this->saveCampaign($request);

    }


    public function storeDynamics(Request $request, $uuid)
    {
        return $this->saveDynamics($request, $uuid);
    }


    public function updateCampaign(Request $request, $uuid)
    {
        return $this->saveCampaign($request, $uuid);
    }
    

    public function deleteCampaign($uuid)
    {
        
        if(!$uuid) {
            return [
                'message' => 'Error deleting campaign',
                'error' => true
            ];
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        try {
            // update active flag
            $campaign->active = 0;
            $campaign->save();

            $campaign->delete();

            return [
                'message' => 'Campaign deleted.',
                'error' => false
            ];


        } catch (\Exception $e) {
            return [
                'message' => 'Error deleting campaign',
                'error' => true
            ];
        }

    }


    /**
     * Stops a campaign by setting the active flag to 0
     */
    public function stopCampaign($uuid)
    {
        if(!$uuid) {
            return [
                'message' => 'Campaign not found',
                'error' => true
            ];
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        if($this->deactivateCampaign($campaign)) {
            
            return [
                'message' => 'Campaign stopped',
                'error' => false
            ];

        } else {

            return [
                'message' => 'Error stopping campaign',
                'error' => true
            ];

        }

    }


    /**
     * Starts a campaign by setting the active flag to 1
     */
    public function startCampaign($uuid)
    {
        if(!$uuid) {
            return [
                'message' => 'Campaign not found',
                'error' => true
            ];
        }

        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        if($this->activateCampaign($campaign)) {
            
            return [
                'message' => 'Campaign started',
                'error' => false
            ];

        } else {

            return [
                'message' => 'Error starting campaign',
                'error' => true
            ];

        }

    }



    /**
     * Deactivate a campaign
     */
    private function deactivateCampaign($campaign)
    {
        try {
            
            $campaign->active = 0;
            $campaign->save();

            return true;
            
        } catch (\Exception $e) {
            return false;
        }
        
    }


    /**
     * Activate a campaign
     */
    private function activateCampaign($campaign)
    {
        try {
            
            $campaign->active = 1;
            $campaign->save();

            return true;
            
        } catch (\Exception $e) {
            return false;
        }
        
    }



    private function saveCampaign(Request $request, $uuid = null)
    {
        

        $data = $request->all();

        if($uuid) {

            $validation = \Validator::make($data, [
                'name' => 'required'
            ]);
            
            if($validation->fails()) {
                return [
                    'formErrors' => $validation->errors(),
                    'message' => "There are errors with the form",
                    'error' => true
                ];
            }
        
            // updating
            $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

            $campaign->name = $data['name'];
            if( isset($data['description']) && $data['description'] != '') {
                $campaign->description = $data['description'];
            }

            try {
                $campaign->save();
                
                return [
                    'campaignUUID' => $campaign->uuid,
                    'shortUrl' => $campaign->url,
                    'message' => 'Campaign Updated Successfully',
                    'error' => false
                ];

            } catch (\Exception $e) {
                return [
                    'campaignUUID' => null,
                    'shortUrl' => null,
                    'message' => $e->getMessage(),
                    'error' => true
                ];
            }

        } else {
            // new camapign
            $this->validate($request, [
                'name' => 'required',
            ]);

            $campaign = new Campaign;
            $campaign->team_id = Auth::user()->currentTeam()->id;
            $campaign->name = $data['name'];

            if( isset($data['description']) && $data['description'] != '') {
                $campaign->description = $data['description'];
            }

            $campaign->type = $data['type'];

            // create the short URL
            $campaignUrl = $this->shortUrlService->generate();
            
            // check if already exists, and re-generate if it does
            while ( Campaign::where('url', $campaignUrl)->first() ) {
                $campaignUrl = $this->shortUrlService->generate();
            }
            
            $campaign->url = $campaignUrl;

            try {
                $campaign->save();
                
                return [
                    'campaignUUID' => $campaign->uuid,
                    'shortUrl' => $campaign->url,
                    'message' => 'Campaign Saved Successfully',
                    'error' => false
                ];
            } catch (\Exception $e) {
                return [
                    'campaignUUID' => null,
                    'shortUrl' => null,
                    'message' => $e->getMessage(),
                    'error' => true
                ];
            }
        }
    }


    private function saveDynamics(Request $request, $uuid)
    {
		
		if(!$uuid) {
			return [
				'campaignUUID' => null,
                'shortUrl' => null,
                'message' => "An error occurred",
                'error' => true
            ];
        }
		
		$data = $request->all();
		
        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();
        
        switch ($data['type']) {
            case 'abtest':
				$mapper = 'mapAbTest';
				$validator = 'validateAbTest';
                break;

            case 'devicetype':
                $mapper = 'mapDeviceType';
                $validator = 'validateDeviceType';
                break;

            case 'browserlanguage':
                $mapper = 'mapBrowserLanguage';
                $validator = 'validateBrowserLanguage';
                break;

            case 'location':
                $mapper = 'mapLocation';
                $validator = 'validateLocation';
                break;

            case 'visitorlimit':
                $mapper = 'mapVisitorLimit';
                $validator = 'validateVisitorLimit';
				break;
				
			case 'datelimit':
				$mapper = 'mapDateLimit';
				$validator = 'validateDateLimit';
            
        }

		$validation = $this->dynamicsValidator->{$validator}($data);
		
		if($validation->fails()) {
			return [
				'formErrors' => $validation->errors(),
				'message' => "There are errors with the form. Please check all fields are complete.",
                'error' => true
            ];
        }
        
        // record data changes to see if they have affected the results of a campaign
        if($campaign->data != "") {

            $campaignChange = new CampaignChange();
            $campaignChange->campaign_id = $campaign->id;
            $campaignChange->old_data = $campaign->data;
            $campaignChange->new_data = $this->dynamicsMapper->{$mapper}($data);

            $campaignChange->save();
        }
		
		$campaign->data = $this->dynamicsMapper->{$mapper}($data);

        try {
            $campaign->save();

            return [
                'message' => "Campaign saved.",
                'error' => false
            ];

        } catch (\Exception $e) {
            return [
                'campaignUUID' => null,
                'shortUrl' => null,
                'message' => $e->getMessage(),
                'error' => true
            ];
        }


    }
    
}
