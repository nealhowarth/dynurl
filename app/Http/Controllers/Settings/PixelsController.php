<?php

namespace App\Http\Controllers\Settings;

use Auth;
use App\UserPixel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PixelsController extends Controller 
{
    
    public function show()
    {
        return view('settings.pixels.create-pixel');
    }



    /** API Endpoints */
    public function getPixels()
    {
        return [
            'pixels' => Auth::user()->pixels()->get(),
            'error' => false
        ];
    }


    public function storePixels(Request $request)
    {
        
        $data = $request->all();
        
        foreach($data as $key => $value) {

            if( isset($value['id']) ) {

                $userPixel = UserPixel::where('id', $value['id'])->where('user_id', Auth::user()->id)->first();
                $userPixel->type = $key;
                $userPixel->pixel = $value['pixel'];

            } elseif( isset($value['pixel']) ) {

                $userPixel = new UserPixel();

                $userPixel->user_id = Auth::user()->id;
                $userPixel->type = $key;
                $userPixel->pixel = $value['pixel'];
            }
            
            try {
                $userPixel->save();
            } catch (\Exception $e) {
                dd($e->getMessage());
            }

        }

        return [
            'message' => "Pixel details have been updated",
            'error' => false
        ];

    }



}
