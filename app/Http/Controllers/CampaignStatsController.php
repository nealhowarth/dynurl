<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Campaign;
use App\CampaignVisit;
use App\CampaignChange;
use App\Services\ShortUrl;
use App\Services\Analytics;

class CampaignStatsController extends Controller
{
    
    private $shortUrlService;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }


    public function home($uuid)
    {
        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();
        
        return view('campaigns.stats.'.$campaign->type, compact($campaign));

    }

    public function test()
    {
        $campaign = Campaign::where('id', 8)->where('team_id', Auth::user()->currentTeam()->id)->first();

        $startDate = \Carbon\Carbon::today()->subDays(6)->format('Y-m-d') . " 00:00:00";
        $endDate = \Carbon\Carbon::today()->format('Y-m-d') . " 23:59:59";

        $analyticsService = new Analytics;

        $clicks = $analyticsService->getClicksByDateRange($campaign, $startDate, $endDate);

        $uniques = $analyticsService->getUniqueVisitorsByDateRange($campaign, $startDate, $endDate);

        $browsers = $analyticsService->getBrowsersByDateRange($campaign, $startDate, $endDate);

        $os = $analyticsService->getOSByDateRange($campaign, $startDate, $endDate);

        $devices = $analyticsService->getDevicesByDateRange($campaign, $startDate, $endDate);

        $language = $analyticsService->getBrowserLanguageByDateRange($campaign, $startDate, $endDate);

        dd($clicks, $uniques, $browsers, $os, $devices, $language);
    }


    /** API Calls */

    public function getCampaignStats($uuid)
    {
        
        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        $data = [];
        
        if($campaign->type == 'abtest') {

            $campaignVisits = $campaignVisits = \DB::select(\DB::raw('
                SELECT chosen_url, count(id) as total
                FROM campaign_visits 
                WHERE campaign_id = :id 
                GROUP BY chosen_url
                '), array('id' => $campaign->id));

            $visits = [];
            $clicksTotal = 0;

            foreach($campaignVisits as $visit) {
                $visits[] = array(
                    'total' => $visit->total > 0 ? $visit->total : 0,
                    'url' => $visit->chosen_url
                );

                $clicksTotal += $visit->total;
                
            }

            $data['data']['abvisits'] = $visits;

        }

        
        $data['data']['total_clicks'] = $clicksTotal;
        $data['campaign'] = $campaign;

        $data['error'] = false;
        $data['message'] = null;

        return $data;

    }


    public function getWeeklyClickStats($uuid)
    {
        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        $data = [];

        $startDate = \Carbon\Carbon::today()->subDays(7)->format('Y-m-d') . " 00:00:00";
        $endDate = \Carbon\Carbon::today()->format('Y-m-d') . " 23:59:59";

        $analyticsService = new Analytics;
        
        if($campaign->type == 'abtest') {

            $campaignVisits = \DB::select(\DB::raw('
                SELECT chosen_url, count(id) as total
                FROM campaign_visits 
                WHERE campaign_id = :id 
                AND DATE(created_at) >= :startDate 
                AND DATE(created_at) <= :endDate 
                GROUP BY chosen_url
                '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

            $visits = [];
            $clicksTotal = 0;

            foreach($campaignVisits as $visit) {
                $visits[] = array(
                    'total' => $visit->total > 0 ? $visit->total : 0,
                    'url' => $visit->chosen_url
                );

                $clicksTotal += $visit->total;
                
            }

            $data['data']['week']['abvisits'] = $visits;
            $data['data']['week']['total_clicks'] = $clicksTotal;

        }

        $allVisits = $analyticsService->getClicksByDateRange($campaign, $startDate, $endDate);

        
        $data['data']['week']['allData'] = $this->formatForBarChart($allVisits);
        

        $data['campaign'] = $campaign;
        $data['error'] = false;
        $data['message'] = null;

        return $data;
    }


    public function getAllClickStats(Request $request, $uuid)
    {
        $data = $request->all();
        
        if(!isset($data['startDate']) || $data['startDate'] == null) {
            $startDate = \Carbon\Carbon::today()->subDays(30)->format('Y-m-d') . " 00:00:00";
        } else {
            $startDate = substr($data['startDate'], 0, -14) . " 00:00:00";
        }

        if(!isset($data['endDate']) || $data['endDate'] == null) {
            $endDate = \Carbon\Carbon::today()->format('Y-m-d') . " 23:59:59";
        } else {
            $endDate = substr($data['endDate'], 0, -14) . " 23:59:59";
        }
        
        $campaign = Campaign::where('uuid', $uuid)->where('team_id', Auth::user()->currentTeam()->id)->first();

        $analyticsService = new Analytics;

        $data = [];

        if($campaign->type == 'abtest') {

            $campaignVisits = \DB::select(\DB::raw('
                SELECT chosen_url, count(id) as total
                FROM campaign_visits 
                WHERE campaign_id = :id 
                AND DATE(created_at) >= :startDate 
                AND DATE(created_at) <= :endDate 
                GROUP BY chosen_url
                '), array('id' => $campaign->id, 'startDate' => $startDate, 'endDate' => $endDate));

            $visits = [];
            $clicksTotal = 0;

            foreach($campaignVisits as $visit) {
                $visits[] = array(
                    'total' => $visit->total > 0 ? $visit->total : 0,
                    'url' => $visit->chosen_url
                );

                $clicksTotal += $visit->total;
                
            }

            $data['data']['allTime']['abvisits'] = $visits;
            $data['data']['allTime']['total_clicks'] = $clicksTotal;

        }

        $allVisits = $analyticsService->getClicksByDateRange($campaign, $startDate, $endDate);

        $data['data']['allTime']['allData'] = $this->formatForBarChart($allVisits);
        

        $data['campaign'] = $campaign;
        $data['error'] = false;
        $data['message'] = null;

        return $data;
    }


    private function formatForBarChart($results)
    {
        
        $data = [];

        foreach($results as $result) {

            $data['xaxis'][] = $result['name'];
            $data['data'][] = $result['clicks'];

        }

        return $data;

    }


}