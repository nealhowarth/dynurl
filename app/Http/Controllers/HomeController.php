<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');

        // $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function show()
    {
        return view('home');
	}
	


    /**
     * Delete a user notification from the pop-out list
     */
	public function deleteNotification(Request $request, $id)
	{
		$user = \Auth::user();

		try {
			$user->notifications()->where('id', $id)->first()->delete();

			return [
				'error' => false,
				'message' => null
			];
			
		} catch (\Exception $e) {
			return [
				'error' => true,
				'message' => 'Error occured: '.$e->getMessage()
			];
		}
	}

}
