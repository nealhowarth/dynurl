<?php

namespace App;

use Laravel\Spark\Team as SparkTeam;

class Team extends SparkTeam
{
    


    public function campaigns()
    {
        return $this->hasMany('App\Campaign');
    }

}
