<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignVisit extends Model
{


    protected $fillable = [
        'chosen_url', 'type', 'ip_address',
        'language_code', 'language_description', 'browser_type',
        'os', 'user_agent', 'device_type', 'latitude', 'longitude',
        'referer', 'visitor_id' 
    ];


    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }


}