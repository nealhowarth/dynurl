<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignChange extends Model
{


    protected $fillable = [
        
    ];


    public function campaign()
    {
        return $this->belongsTo('App\Campaign');
    }

}