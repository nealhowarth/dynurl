<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{

    use SoftDeletes;
    use GeneratesUuid;

    protected $fillable = [
        'uuid', 'name', 'description', 'url', 'type', 'data', 'active'
    ];


    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function campaignChanges()
    {
        return $this->hasMany('App\CampaignChange');
    }

    public function campaignVisits()
    {
        return $this->hasMany('App\CampaignVisit');
    }

}