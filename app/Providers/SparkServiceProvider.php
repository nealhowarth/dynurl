<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'LeapURL',
        'product' => 'LeapURL',
        'street' => 'PO Box 111',
        'location' => 'Your Town, NY 12345',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = "support@leapurl.test";

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'admin@leapurl.test'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::noCardUpFront()->teamTrialDays(-1);

        Spark::noAdditionalTeams();

        Spark::freeTeamPlan()
            ->maxTeamMembers(1)
            ->attributes([
                'campaigns' => 3,
                'campaign_clicks' => 5000,
                'reporting' => 'basic',
                'edit_url' => false
            ])
            ->features([
                '3 Active Campaigns', '5000 Clicks Per Campaign', '1 User', 'Basic Reporting'
            ]);

        Spark::teamPlan('Starter', 'dynurl-starter-1')
            //->archived()
            ->maxTeamMembers(2)
            ->attributes([
                'campaigns' => 3,
                'campaign_clicks' => 15000,
                'reporting' => 'advanced',
                'edit_url' => false
            ])
            ->price(19)
            ->features([
                '3 Active Campaigns', '15,000 Clicks Per Campaign', '2 Users', 'Advanced Reporting'
            ]);

        Spark::teamPlan('Growth', 'dynurl-growth-1')
            //->archived()
            ->maxTeamMembers(3)
            ->attributes([
                'campaigns' => 10,
                'campaign_clicks' => 30000,
                'reporting' => 'advanced',
                'edit_url' => false
            ])
            ->price(49)
            ->features([
                '10 Active Campaigns', '30,000 Clicks Per Campaign', '3 Users', 'Advanced Reporting'
            ]);

        Spark::teamPlan('Business', 'dynurl-business-1')
            ->archived()
            ->maxTeamMembers(10)
            ->attributes([
                'campaigns' => 25,
                'campaign_clicks' => 50000,
                'reporting' => 'advanced',
                'edit_url' => true
            ])
            ->price(89)
            ->features([
                '25 Active Campaigns', '50,000 Clicks Per Campaign', '10 Users', 'Editable Short URLs', 'Advanced Reporting'
            ]);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();
    }
}
