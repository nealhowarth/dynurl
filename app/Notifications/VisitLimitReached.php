<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laravel\Spark\Notifications\SparkChannel;
use Laravel\Spark\Notifications\SparkNotification;

class VisitLimitReached extends Notification
{
	use Queueable;
	
	private $campaign;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SparkChannel::class];
    }
    

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Campaign Click Limit Reached')
                    ->line('You have reached your click limit on '.$this->campaign->name.' campaign')
                    ->action('View Campaign', url('/campaign/edit/'.$this->campaign->uuid));
    }
	

	public function toSpark($notifiable)
	{
		return (new SparkNotification())
					->action('View Campaign', '/campaign/edit/'.$this->campaign->uuid)
					->icon('fa-exclamation')
					->body('You have reached your click limit for the '.$this->campaign->name.' campaign');
	}

    
}
