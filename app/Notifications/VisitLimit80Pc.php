<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Laravel\Spark\Notifications\SparkChannel;
use Laravel\Spark\Notifications\SparkNotification;

class VisitLimit80Pc extends Notification
{
    use Queueable;

    private $campaign;

    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SparkChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Campaign Click Limit Near')
                    ->line('Just a heads up. You are currently at 80% click limit on '.$this->campaign->name.' campaign')
                    ->action('View Campaign', url('/campaign/edit/'.$this->campaign->uuid))
                    ->line('Thanks');
    }


    /**
     * Send an internal notification to the user account
     */
    public function toSpark($notifiable)
	{
		return (new SparkNotification())
					->action('View Campaign', '/campaign/edit/'.$this->campaign->uuid)
					->icon('fa-exclamation')
					->body('Your click limit is at 80% for the '.$this->campaign->name.' campaign');
	}
}
