<?php

namespace App\Listeners\Laravel\Spark\Listeners\Subscription;

use App\Events\Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Auth;
use App\Campaign;

class StopActiveCampaigns
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionCancelled  $event
     * @return void
     */
    public function handle(SubscriptionCancelled $event)
    {
        $team = Auth::user()->currentTeam();
        
		$campaigns = Campaign::where('team_id', $team->id)->where('active', 1)->get();
		
		foreach($campaigns as $campaign) {

			$campaign->active(0);
			$campaign->save();

		}
    }
}
