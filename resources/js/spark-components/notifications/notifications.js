var base = require('notifications/notifications');

Vue.component('spark-notifications', {
	mixins: [base],
	

	methods: {
		clearNotification(notification) {

			axios.put('/api/notifications/'+notification.id+'/delete', notification)
			.then(response => {
				
				if(response.data.error == true) {
					//this.errorMessage = response.data.message;
				} else {

					if(response.data.error == false) {
						this.notifications.notifications.splice(this.notifications.notifications.indexOf(notification), 1);
					}

				}
			});

		}
	}

});
