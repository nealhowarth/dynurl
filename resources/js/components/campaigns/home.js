import campaignsMixin from '../mixins/campaignsMixin';
import Bars from 'vuebars';

Vue.component('campaigns-home', {
    props: [],

    components: {
        Bars
    },

    mixins: [campaignsMixin],

    data() {
        return {
            campaigns: [],
            successMessage: null,
            errorMessage: null,
            shortUrl: process.env.MIX_SHORT_URL
        }
    },

    created() {

        this.getCampaigns();

    },

    methods: {

        removeCampaign(campaign) {
			this.deleteCampaign(campaign);
        },

        startACampaign(campaign) {
            this.startCampaign(campaign).then(response => {
                if(response.data.error == true) {
                    this.errorMessage = response.data.message;
                } else {

                    if(response.data.error == false) {
                        campaign.active = 1;
                        this.successMessage = response.data.message;
                    }

                }
            });
            
        },

        stopACampaign(campaign) {
            this.stopCampaign(campaign).then(response => {
                if(response.data.error == true) {
                    this.errorMessage = response.data.message;
                } else {

                    if(response.data.error == false) {
                        campaign.active = 0;
                        this.successMessage = response.data.message;
                    }

                }
            });
        },
        
        readableCampaignType(type) {
            switch (type) {
                case 'abtest':
                    return 'A/B Test'
                    break;

                case 'devicetype':
                    return 'Device Type'
                    break;

                case 'browserlanguage':
                    return 'Language'
                    break;

                case 'location':
                    return 'Location'
                    break;

                case 'visitorlimit':
                    return 'Visit Limit'
					break;
					
				case 'datelimit':
					return 'Date Limit'
					break;
            
            }
        }

    }
});