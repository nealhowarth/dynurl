import campaignsMixin from '../mixins/campaignsMixin';
import VueApexCharts from 'vue-apexcharts'
import Calendar from 'v-calendar/lib/components/calendar.umd'
import DatePicker from 'v-calendar/lib/components/date-picker.umd'

Vue.component('campaign-stats', {
    props: [],

    components: {
        apexchart: VueApexCharts,
        Calendar,
		DatePicker
    },

    mixins: [campaignsMixin],

    data() {
        return {
            campaign: {},
            allstats: {},
            successMessage: null,
			errorMessage: null,
            options: {},
            series: [],
            allOptions: {},
            allSeries: [],
            allStats: {},
            allValues: {},
            startDate: "",
            endDate: "",
            mode: 'single'
        }
    },

    created() {

        this.getCampaignStats();
        this.getAllCampaignStats();

    },

    methods: {

        getCampaignStats() {
            
            var uuid = this.getIdFromUrl(2);

            axios.get('/api/campaign/'+uuid+'/stats')
                .then(response => {
                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {

                        if(response.data.error == false) {
                            this.campaign = response.data.campaign;
                            //this.allstats = response.data.data;
                        }

                    }
                });

        },
        
        getAllCampaignStats(start = null, end = null) {
            
            var uuid = this.getIdFromUrl(2);

            let postData = {};
            postData.startDate = start;
            postData.endDate = end;

            this.allStats = {};

            axios.post('/api/campaign/'+uuid+'/stats/all', postData)
                .then(response => {
                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {

                        if(response.data.error == false) {
							this.allValues = response.data.data.allTime.allData;
                            this.allStats = response.data.data.allTime;
                            console.log(this.allStats.abvisits.length);
                            
                            this.allOptions = {
                                chart: {
                                    id: 'vuechart-example'
                                },
                                xaxis: {
                                    categories: this.allValues.xaxis
                                }
                            };
                                
                            this.allSeries = [{
                                name: 'Clicks',
                                data: this.allValues.data
                            }];
							
                        }

                    }
                });

        },
        
        updateAllReport() {
            if(this.startDate != "" && this.endDate != "") {
                this.getAllCampaignStats(this.startDate, this.endDate);
            }
        }
    }
});