import campaignsMixin from '../mixins/campaignsMixin';
import AbTest from './dynamics/abtest';
import DeviceType from './dynamics/devicetype';
import BrowserLanguage from './dynamics/browserlanguage';
import Location from './dynamics/location';
import VisitorLimit from './dynamics/visitorlimit';
import DateLimit from './dynamics/datelimit';

Vue.component('campaign-create-dynamics', {
    props: ['user'],
    
    mixins: [campaignsMixin],

	data() {
        return {
			campaign: {},
            device: [],
            language: [],
            location: [],
            successMessage: null,
			errorMessage: null,
			formerrors: {},
			urlEditDisabled: true
        }
    },

    created() {

        this.getCampaign();
        this.checkUrlEditDisabled();

	},
	
    methods: {

        checkUrlEditDisabled() {

            if(this.user.current_billing_plan != null) {
                this.urlEditDisabled = false;
            }

        },

        createDynamics() {
			this.saveDynamics();
        }

    }
});