import campaignsMixin from '../mixins/campaignsMixin';

Vue.component('campaign-edit', {
    props: ['user'],

    mixins: [campaignsMixin],

    data() {
        return {
            campaign: {},
            successMessage: null,
            errorMessage: null,
            formerrors: {}
        }
    },

    created() {

        this.getCampaign(); 

    },

    methods: {

        updateCampaign() {
            this.editCampaign()
        }

    }
});