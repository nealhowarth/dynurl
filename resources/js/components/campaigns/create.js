import campaignsMixin from '../mixins/campaignsMixin';

Vue.component('campaign-create', {
    props: [],

    mixins: [campaignsMixin],

    data() {
        return {
            campaign: new SparkForm ({
                "name": "",
                "description": "",
                "type": ""
            }),
            successMessage: null,
            errorMessage: null
        }
    },

    created() {

        

    },

    methods: {

        createCampaign() {
            this.saveCampaign();
        }

    }
});