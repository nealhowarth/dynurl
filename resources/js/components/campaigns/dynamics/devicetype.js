Vue.component('device-type', {
	
	props: ['campaign', 'formerrors', 'successMessage'],
	
	data() {
        return {
			devices: [
				{
					type: '',
					url: ''
				}
			]
        }
	},

    created() {

		if(this.campaign.devices && this.campaign.devices.length) {
			this.devices = this.campaign.devices;
		}

    },
	

	methods: {

		createDeviceTypeDynamics() {
			this.campaign.devices = this.devices;
			this.$parent.createDynamics();
		},
		
		addDevice() {
			this.devices.push({
				type: '',
				url: ''
			});
		},

		removeDevice(index) {
			if( (this.devices.length - 1) == 0 ) {
				alert("At least one device is required");
				return false;
			}
			this.devices.splice(index, 1);
		}

	}

});

