Vue.component('ab-test', {
	
	props: ['campaign', 'formerrors', 'successMessage'],
	
	data() {
        return {
			
        }
	},

    created() {

		

    },
	

	methods: {

		updatePercentagesFromA(v) {
			this.campaign.apercent = parseInt(v);
			if(isNaN(this.campaign.apercent)) {
				this.campaign.apercent = null;
			}
			this.campaign.bpercent = parseInt(100 - this.campaign.apercent);
		},

		updatePercentagesFromB(v) {
			this.campaign.bpercent = parseInt(v);
			if(isNaN(this.campaign.bpercent)) {
				this.campaign.bpercent = null;
			}
			this.campaign.apercent = parseInt(100 - this.campaign.bpercent);
		},

		createAbTestDynamics() {
			this.$parent.createDynamics();
        }

	}

});

