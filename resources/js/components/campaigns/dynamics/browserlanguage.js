import browserLanguageMixin from '../../mixins/browserLanguageMixin';

Vue.component('browser-language', {
	
    props: ['campaign', 'formerrors', 'successMessage'],
    
    mixins: [browserLanguageMixin],
	
	data() {
        return {
			languages: [
				{
					type: '',
					url: ''
				}
            ],
            languageList: {}
        }
	},

    created() {

		if(this.campaign.languages && this.campaign.languages.length) {
			this.languages = this.campaign.languages;
        }
        
        this.languageList = this.getLanguageList();

    },
	

	methods: {

		createBrowserLanguageDynamics() {
            this.campaign.languages = this.languages;
			this.$parent.createDynamics();
		},
		
		addLanguage() {
			this.languages.push({
				type: '',
				url: ''
			});
		},

		removeLanguage(index) {
			if( (this.languages.length - 1) == 0 ) {
				alert("At least one language is required");
				return false;
			}
			this.languages.splice(index, 1);
		}

	}

});

