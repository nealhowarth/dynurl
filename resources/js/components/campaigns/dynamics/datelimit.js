import Calendar from 'v-calendar/lib/components/calendar.umd'
import DatePicker from 'v-calendar/lib/components/date-picker.umd'

Vue.component('date-limit', {
	
	props: ['campaign', 'formerrors', 'successMessage'],

	components: {
		Calendar,
		DatePicker
	},
	
	data() {
        return {
			mode: 'single'
        }
	},

    created() {

		if(this.campaign.startDate && this.campaign.startDate.length) {
			this.campaign.startDate = new Date(this.campaign.startDate);
		}

		if(this.campaign.endDate && this.campaign.endDate.length) {
			this.campaign.endDate = new Date(this.campaign.endDate);
		}

    },
	

	methods: {

		createDateLimitDynamics() {
			this.$parent.createDynamics();
        }

	}

});

