import locationMixin from '../../mixins/locationMixin';

Vue.component('user-location', {
	
    props: ['campaign', 'formerrors', 'successMessage'],
    
    mixins: [locationMixin],
	
	data() {
        return {
			locations: [
				{
					type: '',
					url: ''
				}
            ],
            laocationList: {}
        }
	},

    created() {

		if(this.campaign.locations && this.campaign.locations.length) {
			this.locations = this.campaign.locations;
        }
        
        this.locationList = this.getLocationList();

    },
	

	methods: {

		createLocationDynamics() {
            this.campaign.locations = this.locations;
			this.$parent.createDynamics();
		},
		
		addLocation() {
			this.locations.push({
				type: '',
				url: ''
			});
		},

		removeLocation(index) {
			if( (this.locations.length - 1) == 0 ) {
				alert("At least one location is required");
				return false;
			}
			this.locations.splice(index, 1);
		}

	}

});

