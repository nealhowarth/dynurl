import campaignsMixin from '../mixins/campaignsMixin';
import AbTest from './dynamics/abtest';
import DeviceType from './dynamics/devicetype';
import BrowserLanguage from './dynamics/browserlanguage';
import Location from './dynamics/location';
import VisitorLimit from './dynamics/visitorlimit';
import DateLimit from './dynamics/datelimit';

import { VTooltip, VPopover, VClosePopover } from 'v-tooltip';

Vue.component('campaign-edit-dynamics', {
    props: ['user'],
    
    mixins: [campaignsMixin],

	data() {
        return {
            campaign: {},
            devices: [],
            languages: [],
            locations: [],
            successMessage: null,
			errorMessage: null,
			formerrors: {},
			urlEditDisabled: true
        }
    },

    created() {

        this.getDynamicsCampaign();
        
        this.checkUrlEditDisabled();

	},
	
    methods: {

        checkUrlEditDisabled() {

            if(this.user.current_billing_plan != null) {
                this.urlEditDisabled = false;
            }

        },

        createDynamics() {
			this.saveDynamics();
        }

    }
});