
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Components
 |--------------------------------------------------------------------------
 |
 | Here we will load the Spark components which makes up the core client
 | application. This is also a convenient spot for you to load all of
 | your components that you write while building your applications.
 */

require('./../spark-components/bootstrap');

require('./home');

require('./campaigns/home');
require('./campaigns/stats');
require('./campaigns/create');
require('./campaigns/createDynamics');
require('./campaigns/edit');
require('./campaigns/editDynamics');
require('./campaigns/dynamics/abtest');
require('./campaigns/dynamics/location');
require('./campaigns/dynamics/datelimit');
require('./campaigns/dynamics/devicetype');
require('./campaigns/dynamics/visitorlimit');
require('./campaigns/dynamics/browserlanguage');

require ('./settings/pixels/createPixel');
