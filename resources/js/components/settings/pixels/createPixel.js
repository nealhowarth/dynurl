
Vue.component('create-pixel', {
    props: [],

    mixins: [],

    data() {
        return {
            pixels: {
                google : {
                    pixel: null
                },
                facebook : {
                    pixel: null
                }
            },
            successMessage: null,
            errorMessage: null,
            busy: false,
        }
    },

    created() {

        this.getPixels();

    },

    methods: {

        getPixels() {
           this.busy = true;
           axios.get('/api/settings/pixels')
            .then(response => {
                this.busy = false;
                if(response.data.error == true) {
                    this.errorMessage = response.data.message;
                } else {
                    
                    if(response.data.error == false) {
                        
                        let thePixels = response.data.pixels;

                        for (let i = 0; i < thePixels.length; i++) {
                            
                            switch (thePixels[i].type) {
                                case 'google':
                                    this.pixels.google = thePixels[i];
                                    break;

                                case 'facebook':
                                    this.pixels.facebook = thePixels[i];
                                    break;
                            
                            }

                        }
                        
                    }

                }

            });
        },
        
        savePixels() {
            this.busy = true;

            axios.post('/api/settings/pixels', this.pixels)
            .then(response => {
                this.busy = false;
                if(response.data.error == true) {
                    this.errorMessage = response.data.message;
                } else {
                    
                    if(response.data.error == false) {
                        this.successMessage = response.data.message;
                        this.getPixels();
                    }

                }

            });

        }

    }
});