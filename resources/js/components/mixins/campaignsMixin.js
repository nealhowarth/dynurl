export default {

    methods: {

        getCampaigns() {
            axios.get(`/api/campaigns/all`)
                .then(response => {
                    this.campaigns = response.data;
                });
        },

        getCampaign() {
            
            var uuid = this.getIdFromUrl(3);
            
            axios.get('/api/campaign/'+uuid)
                .then(response => {

                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {
                        
                        if(response.data.error == false) {
                            this.campaign = response.data.campaign;
                            this.campaign.url = process.env.MIX_SHORT_URL + this.campaign.url
                            
                        }

                    }

                });
        },

        getDynamicsCampaign() {
            
            var uuid = this.getIdFromUrl(3);
            
            axios.get('/api/campaign/'+uuid)
                .then(response => {

                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {
                        
                        if(response.data.error == false) {
                            this.campaign = response.data.campaign;
                            this.campaign.url = process.env.MIX_SHORT_URL + this.campaign.url
                            this.campaign = {...this.campaign, ...JSON.parse(this.campaign.data)};
                            Vue.delete(this.campaign, 'data');
                            
                        }

                    }

                });
        },

        saveCampaign() {
            axios.post('/api/campaign/create', this.campaign)
                .then(response => {
                    
                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {

                        if(response.data.shortUrl) {
                            window.location.href = '/campaigns/create/' + response.data.campaignUUID;
                        }

                    }
                });
        },

        editCampaign() {
            axios.post('/api/campaign/edit/'+this.campaign.uuid, this.campaign)
                .then(response => {
                    
                    if(response.data.error == true) {
                        
                        this.errorMessage = response.data.message;
                        this.formerrors = response.data.formErrors;

                    } else {
                        
                        if(response.data.error == false) {
                            window.location.href = '/campaign/edit/' + this.campaign.uuid + '/dynamics';
                        }

                    }
                });
        },

        saveDynamics() {
			this.errorMessage = null;
			this.successMessage = null;
			
			axios.post('/api/campaign/create/'+this.campaign.uuid, this.campaign)
                .then(response => {

                    if(response.data.error == true) {
						
						this.errorMessage = response.data.message;
						this.formerrors = response.data.formErrors;
						
                    } else {
                        
                        if(response.data.error == false) {
							this.successMessage = response.data.message;
							setTimeout(function(){
								window.location.replace('/campaigns');
							}, 1500)
                            
                        }

                    }

                });
        },

        deleteCampaign(campaign) {
			this.errorMessage = null;
			this.successMessage = null;

            if(confirm('Are you sure you want to delete this campaign?')) {

                axios.put('/api/campaign/'+campaign.uuid+'/delete', campaign)
                .then(response => {
					
                    if(response.data.error == true) {
                        this.errorMessage = response.data.message;
                    } else {

                        if(response.data.error == false) {
                            this.campaigns.splice(this.campaigns.indexOf(campaign), 1);
                            this.successMessage = response.data.message;
                        }

                    }
                });

            }
        },

        stopCampaign(campaign) {
			this.errorMessage = null;
			this.successMessage = null;

            if(confirm('Are you sure you want to stop this campaign?')) {

                return axios.put('/api/campaign/'+campaign.uuid+'/stop', campaign);

            }
        },

        startCampaign(campaign) {
			this.errorMessage = null;
			this.successMessage = null;

            if(confirm('Are you sure you want to re-start this campaign?')) {

                return axios.put('/api/campaign/'+campaign.uuid+'/start', campaign);

            }
        },
        
        backDynamics() {
            window.location.replace('/campaign/edit/'+this.campaign.uuid);
        },

        getIdFromUrl(loc) {
            return window.location.pathname.split('/')[loc];
        },

        copyToClipboard() {
            var copyurl = document.querySelector('#defaultUrlCopy');
            copyurl.setAttribute('type', 'text');
            copyurl.select();

            try {
                var successful = document.execCommand('copy');
                alert('Short URL copied');
              } catch (err) {
                alert('Oops, unable to copy');
              }

            copyurl.setAttribute('type', 'hidden')
            window.getSelection().removeAllRanges()
        }

    }

};