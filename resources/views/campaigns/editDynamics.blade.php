@extends('spark::layouts.app')

@section('content')
<campaign-edit-dynamics :user="user" inline-template>
    <div class="container-fluid">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="alert alert-success" v-if="successMessage">
                    @{{ successMessage }}
                </div>

                <div class="alert alert-danger" v-if="errorMessage">
                    @{{ errorMessage }}
                </div>


                <div class="card card-default">
                    <div class="card-header">@{{ campaign.name }}
                    <a href="/campaigns" class="btn btn-secondary btn-xs pull-right">Cancel</a>
                    </div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12 mb-5">
                            
                                <div class="form-group">
                                    <label for="shortUrl" class="mb-3">Short URL 
									<button class="badge badge-info" v-tooltip.right-start="'This URL should be used to redirect visitors to your prefered location'">?</button></label>
                                    <div class="input-group mb-3">
                                        <input type="text" name="shortUrl" class="form-control" v-model="campaign.url" :disabled="urlEditDisabled">
                                        <input type="hidden" :value="campaign.url" id="defaultUrlCopy">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white link-pointer" @click="copyToClipboard">Copy</span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'abtest'">
                                @include('campaigns/dynamics/abtest')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'devicetype'">
                                @include('campaigns/dynamics/devicetype')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'browserlanguage'">
                                @include('campaigns/dynamics/browserlanguage')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'location'">
                                @include('campaigns/dynamics/location')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'visitorlimit'">
                                @include('campaigns/dynamics/visitorlimit')
                            </div>

							<div class="col-md-12" v-if="campaign.type == 'datelimit'">
                                @include('campaigns/dynamics/datelimit')
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-secondary pull-left" @click="backDynamics()">Back</button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</campaign-edit-dynamics>
@endsection
