<device-type inline-template :campaign="campaign" :formerrors="formerrors">
<div class="row">

	<div class="col-md-12"><h3 class="mb-3 mt-2">Device Type</h3></div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="defaultUrl">Choose A Default URL</label>
			<input type="text" name="defaultUrl" class="form-control" v-model="campaign.defaultUrl" placeholder="Default URL">
			<span class="help-block text-danger" v-show="formerrors.defaultUrl">@{{ formerrors.defaultUrl ? formerrors.defaultUrl[0] : '' }}<br /></span>
			<small>If no devices match your chosen options, visitors will be redirected to the default URL</small>
        </div>

    </div>

	
	<div class="col-md-12" v-for="(device, index) in devices" :key="index">

		<div class="device-types mb-5">
			<div class="col-md-12">
				<div class="form-group">
					<label for="devices">Choose A Device Type</label><button v-if="devices.length > 1" @click="removeDevice(index)" class="btn btn-danger btn-sm btn-round pull-right">-</button>
					<select class="form-control" name="type" v-model="device.type">
						<option value="desktop">Desktop</option>
						<option value="phone">Phone</option>
						<option value="tablet">Tablet</option>
					</select>

					
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="url">URL</label>
					<input type="text" name="url" class="form-control" v-model="device.url" placeholder="URL">
					
				</div>
			</div>

		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<button @click="addDevice" type="button" class="btn btn-secondary">Add device</button>
		</div>
	</div>

	


	<div class="col-md-12">
		<button class="btn btn-success pull-right" @click="createDeviceTypeDynamics()" :disabled="successMessage">Save</button>
	</div>

    
</div>
</device-type>