<visitor-limit inline-template :campaign="campaign" :formerrors="formerrors">
<div class="row">

	<div class="col-md-12"><h3 class="mb-3 mt-2">Visitor Limits</h3></div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="defaultUrl">Choose A Default URL</label>
			<input type="text" name="defaultUrl" class="form-control" v-model="campaign.defaultUrl" placeholder="Default URL">
			<span class="help-block text-danger" v-show="formerrors.defaultUrl">@{{ formerrors.defaultUrl ? formerrors.defaultUrl[0] : '' }}<br /></span>
			<small>After hitting the maximum visits, a visitor will be directed to this website.</small>
        </div>

    </div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="limit">Maximum Visits Limit</label>
			<input type="number" name="limit" class="form-control" v-model="campaign.limit">
        </div>

    </div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="url">URL</label>
			<input type="text" name="limiturl" class="form-control" v-model="campaign.limiturl">
        </div>

    </div>


	<div class="col-md-12">
		<button class="btn btn-success pull-right" @click="createVisitorLimitDynamics()" :disabled="successMessage">Save</button>
	</div>

	
</div>
</visitor-limit>