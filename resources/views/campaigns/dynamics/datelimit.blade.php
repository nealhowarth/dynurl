<date-limit inline-template :campaign="campaign" :formerrors="formerrors">
<div class="row">

	<div class="col-md-12"><h3 class="mb-3 mt-2">Date Limit</h3></div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="defaultUrl">Choose A Default URL</label>
			<input type="text" name="defaultUrl" class="form-control" v-model="campaign.defaultUrl" placeholder="Default URL">
			<span class="help-block text-danger" v-show="formerrors.defaultUrl">@{{ formerrors.defaultUrl ? formerrors.defaultUrl[0] : '' }}<br /></span>
			<small>If the click date is not within the date range, a visitor will be directed to this website.</small>
        </div>

    </div>

    <div class="col-md-6 mb-3">
                                
        <div class="form-group">
            <label for="startDate">Start Date</label>
			<date-picker :mode='mode' :value="campaign.startDate" :min-date="new Date()" v-model='campaign.startDate' />
        </div>

    </div>

	<div class="col-md-6 mb-3">
                                
        <div class="form-group">
            <label for="endDate">End Date</label>
			<date-picker :mode='mode' :min-date="campaign.startDate ? campaign.startDate : new Date()" v-model='campaign.endDate' />
        </div>

    </div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="url">URL</label>
			<input type="text" name="limiturl" class="form-control" v-model="campaign.visitUrl">
        </div>

    </div>


	<div class="col-md-12">
		<button class="btn btn-success pull-right" @click="createDateLimitDynamics()" :disabled="successMessage">Save</button>
	</div>

	
</div>
</date-limit>