<user-location inline-template :campaign="campaign" :formerrors="formerrors">
<div class="row">

	<div class="col-md-12"><h3 class="mb-3 mt-2">Location</h3></div>

    <div class="col-md-12 mb-3">
                                
        <div class="form-group">
            <label for="defaultUrl">Choose A Default URL</label>
			<input type="text" name="defaultUrl" class="form-control" v-model="campaign.defaultUrl" placeholder="Default URL">
			<span class="help-block text-danger" v-show="formerrors.defaultUrl">@{{ formerrors.defaultUrl ? formerrors.defaultUrl[0] : '' }}<br /></span>
			<small>If no browser languages match your chosen options, visitors will be redirected to the default URL</small>
        </div>

    </div>

	
	<div class="col-md-12" v-for="(location, index) in locations" :key="index">

		<div class="device-types mb-5">
			<div class="col-md-12">
				<div class="form-group">
					<label for="devices">Choose A Location</label><button v-if="locations.length > 1" @click="removeLocation(index)" class="btn btn-danger btn-sm btn-round pull-right">-</button>
					<select class="form-control" name="type" v-model="location.type">
						<option v-for="loc in locationList" v-bind:value="loc.code">@{{ loc.name }}</option>
					</select>

					
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<label for="url">URL</label>
					<input type="text" name="url" class="form-control" v-model="location.url" placeholder="URL">
					
				</div>
			</div>

		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<button @click="addLocation" type="button" class="btn btn-secondary">Add location</button>
		</div>
	</div>

	


	<div class="col-md-12">
		<button class="btn btn-success pull-right" @click="createLocationDynamics()" :disabled="successMessage">Save</button>
	</div>

    
</div>
</user-location>