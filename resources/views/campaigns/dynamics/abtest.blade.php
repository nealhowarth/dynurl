<ab-test inline-template :campaign="campaign" :formerrors="formerrors">
<div class="row">

	<div class="col-md-12"><h3 class="mb-3 mt-2">A/B Test</h3></div>

    <div class="col-md-6">
                                
        <div class="form-group">
            <label for="apercent">A</label>
			<div class="input-group mb-3">
				<input type="text" name="apercent" class="form-control" v-model="campaign.apercent" placeholder="A Percentage" @keyup="updatePercentagesFromA(campaign.apercent)">
				<div class="input-group-append">
					<span class="input-group-text">%</span>
				</div>
            </div>
			<span class="help-block text-danger" v-show="formerrors.apercent">@{{ formerrors.apercent ? formerrors.apercent[0] : '' }}</span>
				<br /><br />
			<input type="text" name="aurl" class="form-control" v-model="campaign.aurl" placeholder="A URL">
			<span class="help-block text-danger" v-show="formerrors.aurl">@{{ formerrors.aurl ? formerrors.aurl[0] : '' }}</span>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label for="bpercent">B</label>
			<div class="input-group mb-3">
            	<input type="text" name="bpercent" class="form-control" v-model="campaign.bpercent" placeholder="B Percentage" @keyup="updatePercentagesFromB(campaign.bpercent)">
				<div class="input-group-append">
					<span class="input-group-text">%</span>
				</div>
            </div>
			<span class="help-block text-danger" v-show="formerrors.apercent">@{{ formerrors.bpercent ? formerrors.bpercent[0] : '' }}</span>
			<br /><br />
            <input type="text" name="burl" class="form-control" v-model="campaign.burl" placeholder="B URL">
			<span class="help-block text-danger" v-show="formerrors.burl">@{{ formerrors.burl ? formerrors.burl[0] : '' }}</span>
        </div>

    </div>


	<div class="col-md-12">
		<button class="btn btn-success pull-right" @click="createAbTestDynamics()" :disabled="successMessage">Save</button>
	</div>

	
</div>
</ab-test>