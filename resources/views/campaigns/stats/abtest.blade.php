@extends('spark::layouts.app')

@section('content')
<campaign-stats :user="user" inline-template>
    <div class="container-fluid">
        <!-- Campaign Reporting -->
        <div class="row justify-content-center">

            <div class="col-md-10">
                <div class="card card-default">
                    <div class="card-header">{{__('All Stats For')}} @{{ campaign.name }}<span class="pull-right">@{{ campaign.url }}</span></div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12 mb-5">
                                <div class="row">
                                    <div class="col-md-2"><date-picker :mode='mode' :max-date="new Date()" v-model='startDate' /></div>
                                    <div class="col-md-2"><date-picker :mode='mode' :min-date="startDate ? startDate : null" :max-date="new Date()" v-model='endDate' /></div>
                                    <div class="col-md-1"><button class="btn btn-primary" @click="updateAllReport()">Update</button></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="card card-default">
                                    <div class="card-header text-center">{{__('A/B Test')}}</div>
                                    <div class="card-body report-card-body">

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="card card-default" v-if="!allStats.abvisits || !allStats.abvisits.length > 0">
                                                    <div class="card-header text-center">
                                                        <strong>A</strong><br />
                                                    </div>
                                                    <div class="card-body text-center">
                                                        0
                                                    </div>
                                                </div>
                                                
                                                <div class="card card-default" v-if="allStats.abvisits && allStats.abvisits.length > 0">
                                                    <div class="card-header text-center">
                                                        <strong>A</strong><br />
                                                        <small>@{{ allStats.abvisits[0].url }}</small>
                                                    </div>
                                                    <div class="card-body text-center">
                                                        @{{ allStats.abvisits[0].total }}
                                                    </div>
                                                </div>

                                                

                                            </div>

                                            <div class="col-md-6">
                                                <div class="card card-default" v-if="allStats.abvisits && allStats.abvisits.length === 0">
                                                    <div class="card-header text-center">
                                                        <strong>B</strong><br />
                                                    </div>
                                                    <div class="card-body text-center">
                                                        0
                                                    </div>
                                                </div>
                                                
                                                <div class="card card-default" v-if="allStats.abvisits && allStats.abvisits.length > 1">
                                                    <div class="card-header text-center">
                                                        <strong>B</strong><br />
                                                        <small>@{{ allStats.abvisits[1].url }}</small>
                                                    </div>
                                                    <div class="card-body text-center">
                                                        @{{ allStats.abvisits[1].total }}
                                                    </div>
                                                </div>

                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            
                            <div class="col-md-2">
                                <div class="card card-default">
                                    <div class="card-header text-center">{{__('Total Clicks')}}</div>
                                    <div class="card-body text-center report-card-body" v-if="allStats.total_clicks">
                                        <h1 v-if="allStats.total_clicks">@{{ allStats.total_clicks }}</h1>
                                    </div>
									<div class="card-body text-center report-card-body" v-if="!allStats.total_clicks">
                                        <h1>0</h1>
                                    </div>
                                </div>
                            </div>
                        

                            <div class="col-md-6">
                                <div class="card card-default">
                                    <div class="card-header text-center">{{__('All Time Clicks')}}</div>
                                    <div class="card-body text-center chart-6-400h">
                                        
                                            <apexchart height="200" type="bar" :options="allOptions" :series="allSeries"></apexchart>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</campaign-stats>
@endsection
