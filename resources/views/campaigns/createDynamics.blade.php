@extends('spark::layouts.app')

@section('content')
<campaign-create-dynamics :user="user" inline-template>
    <div class="container-fluid">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="alert alert-success" v-if="successMessage">
                    @{{ successMessage }}
                </div>

                <div class="alert alert-danger" v-if="errorMessage">
                    @{{ errorMessage }}
                </div>


                <div class="card card-default">
                    <div class="card-header">@{{ campaign.name }}</div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label for="shortUrl">Short URL
										<button class="btn-info btn-sm" v-tooltip.right-start="'This URL should be used to redirect visitors to your prefered location'">?</button>
									</label>
                                    <input type="text" name="shortUrl" class="form-control" v-model="campaign.url" :disabled="urlEditDisabled">
                                </div>

                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'abtest'">
                                @include('campaigns/dynamics/abtest')
                            </div>

							<div class="col-md-12" v-if="campaign.type == 'devicetype'">
                                @include('campaigns/dynamics/devicetype')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'browserlanguage'">
                                @include('campaigns/dynamics/browserlanguage')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'location'">
                                @include('campaigns/dynamics/location')
                            </div>

                            <div class="col-md-12" v-if="campaign.type == 'visitorlimit'">
                                @include('campaigns/dynamics/visitorlimit')
                            </div>

							<div class="col-md-12" v-if="campaign.type == 'datelimit'">
                                @include('campaigns/dynamics/datelimit')
                            </div>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</campaign-create-dynamics>
@endsection
