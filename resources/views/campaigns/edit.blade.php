@extends('spark::layouts.app')

@section('content')
<campaign-edit :user="user" inline-template>
    <div class="container-fluid">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="alert alert-success" v-if="successMessage">
                    @{{ successMessage }}
                </div>

                <div class="alert alert-danger" v-if="errorMessage">
                    @{{ errorMessage }}
                </div>


                <div class="card card-default">
                    <div class="card-header">{{__('Edit Campaign')}}
                    <a href="/campaigns" class="btn btn-secondary btn-xs pull-right">Cancel</a>
                    </div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label for="name">Give your campaign a name</label>
                                    <input type="text" name="name" v-model="campaign.name" class="form-control">
                                    <span class="help-block text-danger" v-show="formerrors.name">@{{ formerrors.name ? formerrors.name[0] : '' }}</span>
                                </div>

                            </div>


                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label for="description">Give it a description (optional)</label>
                                    <input type="text" name="description" v-model="campaign.description" class="form-control">
                                </div>

                            </div>


                            <div class="col-md-12">
                        
                                <div class="form-group">
                                    <label for="name">Campaign Type</label>
                                    <select class="form-control" v-model="campaign.type" disabled>
                                        <option value="abtest">A/B Test</option>
                                        <option value="devicetype">Device Type</option>
                                        <option value="browserlanguage">Browser Language</option>
                                        <option value="location">Location</option>
                                        <option value="visitorlimit">Visitor Limit</option>
										<option value="datelimit">Date Limit</option>
                                    </select>
                                </div>

                            </div>


                            <div class="col-md-12">
                                <button class="btn btn-success pull-right" @click="updateCampaign()" :disabled="successMessage">Next</button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</campaign-edit>
@endsection
