@extends('spark::layouts.app')

@section('content')
<campaign-create :user="user" inline-template>
    <div class="container-fluid">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="alert alert-success" v-if="successMessage">
                    @{{ successMessage }}
                </div>

                <div class="alert alert-danger" v-if="errorMessage">
                    @{{ errorMessage }}
                </div>


                <div class="card card-default">
                    <div class="card-header">{{__('New Campaign')}}</div>

                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label for="name">Give your campaign a name</label>
                                    <input type="text" name="name" v-model="campaign.name" class="form-control">
                                    <span class="help-block text-danger" v-show="campaign.errors.has('name')">@{{ campaign.errors.get('name') }}</span>
                                </div>

                            </div>


                            <div class="col-md-12">
                            
                                <div class="form-group">
                                    <label for="description">Give it a description (optional)</label>
                                    <input type="text" name="description" v-model="campaign.description" class="form-control">
                                    <span class="help-block text-danger" v-show="campaign.errors.has('description')">@{{ campaign.errors.get('description') }}</span>
                                </div>

                            </div>


                            <div class="col-md-12">
                        
                                <div class="form-group">
                                    <label for="name">Choose Campaign Type</label>
                                    <select class="form-control" v-model="campaign.type">
                                        <option value="abtest">A/B Test</option>
										<option value="devicetype">Device Type</option>
                                        <option value="browserlanguage">Browser Language</option>
                                        <option value="location">Location</option>
                                        <option value="visitorlimit">Visitor Limit</option>
										<option value="datelimit">Date Limit</option>
                                    </select>
                                </div>

                            </div>


                            <div class="col-md-12">
                                <button class="btn btn-success pull-right" @click="createCampaign()" :disabled="successMessage">Next</button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</campaign-create>
@endsection
