@extends('spark::layouts.app')

@section('content')
<campaigns-home :user="user" inline-template>
    <div class="container-fluid">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <div class="col-md-10">

                <div class="alert alert-success" v-if="successMessage">
                    @{{ successMessage }}
                </div>

                <div class="alert alert-danger" v-if="errorMessage">
                    @{{ errorMessage }}
                </div>

                
                <div class="card card-default">
                    <div class="card-header">{{__('Campaigns')}}
                        <a href="/campaign/create" class="btn btn-primary btn-xs pull-right">+ Campaign</a>
                    </div>

                    <div class="card-body">
                        
                        <div class="row">

							<div v-if="!campaigns.length" class="col-md-12">
								<p>You have no campaigns to display.</p>
							</div>
                            
                            <div v-for="campaign in campaigns" class="col-lg-3">
                                <div class="card card-default">
                                    <div class="card-header text-center">
                                        <span class="pull-left fa fa-play text-success" v-if="campaign.active == 1" v-tooltip.right-start="'Campaign running'"></span>
                                        <span class="pull-left fa fa-stop-circle text-warning" v-if="campaign.active == 0" v-tooltip.right-start="'Campaign stopped'"></span>
                                        <strong>@{{ campaign.name }}</strong>
                                        <span class="pull-right fa fa-exclamation-triangle text-danger" v-if="!campaign.data" v-tooltip.right-start="'Campaign not complete'"></span>
                                    </div>
                                    <div class="card-body text-center campaign-card-body">
                                        
                                            <p><small>@{{ shortUrl + campaign.url }}</small></p>
                                            <div v-if="campaign.type == 'abtest'" class="row">
                                                <div class="col-md-6">
                                                    <div class="card card-default">
                                                        <div class="card-header text-center">
                                                            <strong>A</strong>
                                                        </div>
                                                        <div class="card-body text-center">
                                                            @{{ campaign.visits.length ? campaign.visits[0].value : 0 }}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="card card-default">
                                                        <div class="card-header text-center">
                                                            <strong>B</strong>
                                                        </div>
                                                        <div class="card-body text-center">
                                                            @{{ campaign.visits.length ? campaign.visits[1].value : 0 }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <bars
                                            v-if="campaign.visits.length > 1 && campaign.type != 'abtest'"
                                            :data="campaign.visits"
                                            :gradient="['#6fa8dc', '#42b983']">
                                            </bars>
                                            <p v-if="campaign.visits.length > 1 && campaign.type != 'abtest'"><small>Last 7 Days Clicks</small></p>
                                            <span v-if="campaign.visits.length == 1 && campaign.type != 'abtest'"><p>No data to display</p></span>
                                        
                                        
                                        <div class="row force-to-bottom">
                                            <div class="col-md-12">
                                                <p class="pull-left pt8"><span class="badge badge-info">@{{ readableCampaignType(campaign.type) }}</span></p>
                                                
                                                <div class="dropdown">
                                                    <button class="btn round-button pull-right" type="button" id="jobListOptions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-fw fa-btn fa-ellipsis-v"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="jobListOptions">
                                                        
                                                        <a class="dropdown-item" :href="'/campaign/edit/'+campaign.uuid">Edit</a>
                                                        <a class="dropdown-item" :href="'/campaign/'+campaign.uuid+'/stats'">Stats</a>
                                                        
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item text-warning" v-if="campaign.active == 1" @click="stopACampaign(campaign)">Stop</a>
                                                        <a class="dropdown-item" v-if="campaign.active == 0" @click="startACampaign(campaign)">Start</a>
                                                        
                                                        <div class="dropdown-divider"></div>
                                                        <a href="#" @click="removeCampaign(campaign)" class="dropdown-item text-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</campaigns-home>
@endsection
