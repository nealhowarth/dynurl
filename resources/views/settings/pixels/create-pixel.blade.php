<create-pixel inline-template>

<div class="card card-default">
        <div class="card-header">{{__('Pixels')}}</div>

        <div class="card-body">
            <!-- Success Message -->
            <div class="alert alert-success" v-if="successMessage">
                {{__('Your pixel details have been updated!')}}
            </div>

            <form role="form">
                <!-- Name -->
                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">{{__('Google Pixel')}}</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="name" v-model="pixels.google.pixel"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-4 col-form-label text-md-right">{{__('Facebook Pixel')}}</label>

                    <div class="col-md-6">
                        <textarea class="form-control" name="name" v-model="pixels.facebook.pixel"></textarea>
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="savePixels"
                                :disabled="busy">

                            {{__('Update')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</create-pixel>
