<?php
// set the cookie
if( !isset($_COOKIE['dynurl'])) setcookie('dynurl', $visitorId, strtotime('+ 1 year'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>LeapURL</title>
    <?php
        if($pixels) {
            
            if( isset($pixels['google']) ) {
                print $pixels['google'];
            }

            if( isset($pixels['facebook']) ) {
                print $pixels['facebook'];
            }

        }

    ?>
</head>
<body>
    <div class="loader">Redirecting to {{$chosenUrl}}</div>
</body>
</html>	
<?php
header('HTTP/1.1 301 Moved Permanently');
header("Location: {$chosenUrl}", true, 301);
exit();