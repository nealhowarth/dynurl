<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('campaign_id');
            $table->string('type')->default('click');
            $table->string('chosen_url');
            $table->string('referer')->nullable();
            $table->ipAddress('ip_address');
            $table->string('device_type')->nullable();
            $table->string('language_code')->nullable();
            $table->string('language_description')->nullable();
            $table->string('browser_type')->nullable();
            $table->string('os')->nullable();
            $table->string('user_agent')->nullable();
            $table->decimal('latitude', 8, 6)->nullable();
            $table->decimal('longitude', 8, 6)->nullable();
            $table->timestamps();

            $table->foreign('campaign_id')->references('id')->on('campaigns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_visits');
    }
}
